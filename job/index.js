'use strict';
const info = require('../package.json');
const queryService = require('./queryService');
const saveService = require('./saveService');

const handleError = (error, statusCode) => {
  let response = {
    statusCode: statusCode ? statusCode : 500,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(error),
  };
  return response;
};

/* GET crawl_full: money & gold */
module.exports.crawl_status = async(event, context) => {
  try 
  {
    let res_sjc, res_vcb, res_sbv;

    if (event.queryStringParameters) {
      let { code } =  event.queryStringParameters;
      if(code && code.toUpperCase() === 'SJC') res_sjc = await this.crawl_sjc();
      if(code && code.toUpperCase() === 'VCB') res_vcb = await this.crawl_vcb();
      if(code && code.toUpperCase() === 'SBV') res_sbv = await this.crawl_sbv();
    }

    return {
      statusCode: 200,
      body: JSON.stringify({
        message: `crawl status executed successful !!!`,
        result: event.queryStringParameters ? { sjc: res_sjc, vcb: res_vcb, sbv: res_sbv } 
          : { code: true, version: info.version }
      })
    }
  }
  catch(err) {
    return handleError(err);
  }
};

/* Exchange Rate: schedules */
module.exports.crawl_vcb = async () => {
  try {
    let req = {
      body: JSON.stringify({ source: 'vcb.com.vn' })
    };
    return this.saveMoney(req);
  } catch (err) {
    throw err;
  }
};

module.exports.crawl_vcb_interest = async () => {
  try {
    let req = {
      body: JSON.stringify({ source: 'vcb.interest.com.vn' })
    };
    return this.saveInterest(req);
  } catch (err) {
    throw err;
  }
};

module.exports.crawl_sbv = async () => {
  try {
    let req = {
      body: JSON.stringify({ source: 'sbv.gov.vn' })
    };
    return this.saveMoney(req);
  } catch (err) {
    throw err;
  }
};

module.exports.crawl_sjc = async () => {
  try {
    let req = {
      body: JSON.stringify({ source: 'sjc.com.vn' })
    };
    return this.saveGold(req);
  } catch (err) {
    throw err;
  }
};

/* Exchange Rate: saving Money & Gold */
module.exports.saveMoney = async (event, context) => {
  try
  {
    let query = event.body ? JSON.parse(event.body) : {};
    let { source } = query;
    if (!source)
      throw { err_code: 'MISSING_REQUIRED_FIELD', err_name: 'Missing required field: source'};

    if(['sbv.gov.vn', 'vcb.com.vn'].indexOf(source) == -1)
      throw { code: 'ValidationException', message: 'source is invalid' }

    let exData, newItem;
    if(source == 'sbv.gov.vn') {
      exData = await queryService.getSbvExchangeRate();
    }
    else {
      exData = await queryService.getVcbExchangeRate();
    }

    let {
      data: {
        src, usd , eur , jpy
      }, 
      time 
    } = exData;
    newItem = await saveService.saveExchangeRate(
      src, time, 
      'USD', usd.rate, usd.buy, usd.sell, 
      'EUR', eur.rate, eur.buy, eur.sell,
      'JPY', jpy.rate, jpy.buy, jpy.sell);
    
    return newItem;
  }
  catch (err) {
    throw err;
  };
};

module.exports.saveGold = async (event, context) => {
  try
  {
    let query = event.body ? JSON.parse(event.body) : {};
    let { source } = query;
    if (!source)
      throw { err_code: 'MISSING_REQUIRED_FIELD', err_name: 'Missing required field: source'};

    if(['sjc.com.vn'].indexOf(source) == -1)
      throw { code: 'ValidationException', message: 'source is invalid' }

    let exData = await queryService.getSjcGold();

    let {
      data: {
        src,
        sjc: { code , name, rate, buy, sell }
      },
      time
    } = exData;
    let newItem = await saveService.saveGold(src, time, code , name, rate, buy, sell);

    return newItem;
  }
  catch (err) {
    throw err;
  };
};

module.exports.saveInterest = async (event, context) => {
  try
  {
    let query = event.body ? JSON.parse(event.body) : {};
    let { source } = query;
    if (!source)
      throw { err_code: 'MISSING_REQUIRED_FIELD', err_name: 'Missing required field: source'};

    if(['vcb.interest.com.vn'].indexOf(source) == -1)
      throw { code: 'ValidationException', message: 'source is invalid' }
    
    let intData = await queryService.getVcbInterest();

    let { data: { src, ls_1M, ls_6M, ls_12M }, time } = intData;
    let newItem = await saveService.saveInterest(src, time, ls_1M, ls_6M, ls_12M);
    return newItem;
  }
  catch (err) {
    throw err;
  };
};