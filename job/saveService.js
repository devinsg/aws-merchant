const axios = require('axios');

const api_save = axios.create({
    baseURL: 'https://c1rh4pkx3f.execute-api.us-east-1.amazonaws.com'
});

const api_save = require('../api/api_save');

const saveService = function() {
};

saveService.saveExchangeRate = function(
    src, time, 
    usd_code, usd_rate, usd_buy, usd_sell, 
    eur_code, eur_rate, eur_buy, eur_sell, 
    jpy_code, jpy_rate, jpy_buy, jpy_sell)
{
    let url = '/api/exchangerate/add';
    let data = {
        src, time,
        usd_code, usd_rate, usd_buy, usd_sell,
        eur_code, eur_rate, eur_buy, eur_sell,
        jpy_code, jpy_rate, jpy_buy, jpy_sell
    };
    return new Promise((resolve, reject) => {
        api_save.post(url, data)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

saveService.saveInterest = function(src, time, ls_1M, ls_6M, ls_12M)
{
    let url = '/api/interest/add';
    let data = { src, time, ls_1M, ls_6M, ls_12M };
    
    return new Promise((resolve, reject) => {
        api_save.post(url, data)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

saveService.saveGold = function(src, time, code, name, rate, buy, sell) {
    let url = '/api/gold/add';
    let data = { src, time, code, name, rate, buy, sell };
    return new Promise((resolve, reject) => {
        api_save.post(url, data)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

module.exports = saveService;