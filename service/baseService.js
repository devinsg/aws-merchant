const api_dev = require('../api/api_dev');
const api_vnm = require('../api/api_vnm');
const api_kms = require('../api/api_kms');
const api_kms3 = require('../api/api_kms3');

const baseService = function() {
}

baseService.accessToken = function() {
    return new Promise((resolve, reject) => {
        api_dev.get('/accessToken')
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

baseService.accessTokenVnm = function() {
    return new Promise((resolve, reject) => {
        api_vnm.get('/accessToken')
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};



baseService.getXspAllMembers = function(cursor) {
    return new Promise((resolve, reject) => {
        let url = (cursor && cursor != 0)
            ? `/tenant/users?cursor=${cursor}`
            : '/tenant/users';
        
        if (cursor && cursor != 0) {
            return api_kms3.get(url)
            .then((res) => {
                const data = res.data;
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
        } 
        else 
        {
            return api_dev.get('/accessToken')
            .then((res) => {
                const { data: { accessToken } } = res;
                return accessToken;
            })
            .then((token) => {
                api_kms3.defaults.headers.get['accessToken'] = token;
                return api_kms3.get(url);
            })
            .then((res) => {
                const data = res.data;
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
        }
    });
}

baseService.getVnmAllMembers = function(cursor) {
    return new Promise((resolve, reject) => {
        let url = (cursor && cursor != 0)
            ? `/tenant/users?cursor=${cursor}`
            : '/tenant/users';
        
        if (cursor && cursor != 0) {
            return api_kms.get(url)
            .then((res) => {
                const data = res.data;
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
        } 
        else 
        {
            return api_vnm.get('/accessToken')
            .then((res) => {
                const { data: { accessToken } } = res;
                return accessToken;
            })
            .then((token) => {
                api_kms.defaults.headers.get['accessToken'] = token;
                return api_kms.get(url);
            })
            .then((res) => {
                const data = res.data;
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
        }
    });
}

module.exports = baseService;