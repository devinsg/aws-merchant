const loginInfo = require('../data/login.json');
const shippingInfo = require('../data/shipping.json');

const Factory = function() {
}

Factory.login = function(username, password) {
    let data = loginInfo || {};
    return new Promise((resolve, reject) => {
        if(data) resolve(data);
        else reject({ username, password });
    });
}

Factory.queryTicketsWithStatus = function(query) {
    let data = shippingInfo || {};
    return new Promise((resolve, reject) => {
        if(data) resolve(data);
        else reject({ query });
    });
}

Factory.shipperUpdateTicket = function(ticketId) {
    return new Promise((resolve, reject) => {
        if(ticketId) resolve({ success: true, ticketId: ticketId });
        else reject({ error: 'NOT FOUND ITEM' });
    });
}

module.exports = Factory;