const axios = require('axios');

const api_query = axios.create({
    baseURL: 'https://api-etherum.azurewebsites.net'
});

const queryService = function() {
};

queryService.status = function() {
    let surl = '/api/status';
    return new Promise((resolve, reject) => {
        api_query.get(surl)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

queryService.getSbvExchangeRate = function() {
    let sbv_url = '/api/money/getsbvexchangefull';
    return new Promise((resolve, reject) => {
        api_query.get(sbv_url)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

queryService.getVcbExchangeRate = function() {
    let vcb_url = '/api/money/getvcbexchange';
    return new Promise((resolve, reject) => {
        api_query.get(vcb_url)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

queryService.getVcbInterest = function() {
    let vcb_interest = '/api/interest/vcb';
    return new Promise((resolve, reject) => {
        api_query.get(vcb_interest)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

queryService.getSjcGold = function() {
    let sjc_url = '/api/gold/getsjcgold';
    return new Promise((resolve, reject) => {
        api_query.get(sjc_url)
        .then((res) => {
            const data = res.data;
            resolve(data);
        })
        .catch((err) => {
            reject(err);
        });
    });
};

module.exports = queryService;