'use strict';
const info = require('../package.json');
const {
  accessToken, accessTokenVnm, getXspAllMembers, getVnmAllMembers, postKaizalaNotification 
} = require('../service/baseService');

const handleError = (error, statusCode) => {
  let response = {
    statusCode: statusCode ? statusCode : 500,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(error),
  };
  return response;
};

module.exports.hello = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: `Go Serverless with Express Middleware v${info.version}! Your function executed successfully !!!`,
      version: info.version,
      input: event
    })
  }
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.refreshToken = async (event, context) => {
  try
  {
    const result = await accessToken();
    const { endpointUrl, accessToken: token } = result;
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        endpointUrl,
        token: token
      })
    }
  }
  catch (err) {
    return handleError(err);
  };
};

module.exports.refreshTokenVnm = async (event, context) => {
  try
  {
    const result = await accessTokenVnm();
    const { endpointUrl, accessToken: token } = result;
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        endpointUrl,
        token: token
      })
    }
  }
  catch (err) {
    return handleError(err);
  };
};



module.exports.getXspAllMembers = async (event, context) => {
  try
  {
    let cursor = event.pathParameters.cursor;
    let result = await getXspAllMembers(cursor);
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(result)
    }
  }
  catch (err) {
    if (err && err.err_code == 'MISSING_ACCESSTOKEN')
      return handleError(err, 401);
    else 
      return handleError(err);
  };
};

module.exports.getVnmAllMembers = async (event, context) => {
  try
  {
    let cursor = event.pathParameters.cursor;
    let result = await getVnmAllMembers(cursor);
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(result)
    }
  }
  catch (err) {
    if (err && err.err_code == 'MISSING_ACCESSTOKEN')
      return handleError(err, 401);
    else 
      return handleError(err);
  };
};

module.exports.postKaizalaNotification = async (event, context) => {
  try
  {
    let data = JSON.parse(event.body);
    let { title, description, content, image } = data;
    title = title ? title : 'Test title on Sunday';
    description = description ? description : 'Test description on Sunday';
    content = content ? content : 'Test content on Sunday';
    image = image ? image : 'https://demo2018.blob.core.windows.net/uploads/img00.jpg';
    let result = await postKaizalaNotification(title, description, content, image);
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(result)
    }
  }
  catch (err) {
    return handleError(err);
  };
};
