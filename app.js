'use strict';
const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const merchants = require('./data/merchants'); // sample data
const express = require('express');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/merchant/list', function (req, res) {
  return res.status(200).json(merchants);
});

app.get('/merchant/item/:id', function (req, res) {
  let id = req.params.id;
  let merchant = merchants.find((item, index) => { if((index+1) == id) return item; });
  return res.status(200).json({merchant});
});

app.post('/merchant/create', function (req, res) {
  let {code, name} = req.body;
  return res.status(200).json({
    success: true,
    merchant: {code, name}
  });
});

app.put('/merchant/update', function (req, res) {
  let {id, code, name} = req.body;
  return res.status(200).json({
    success: true,
    merchant: {id, code, name}
  });
});

app.delete('/merchant/delete', function (req, res) {
  let {id} = req.body;
  return res.status(200).json({
    success: true,
    merchant: {id}
  });
});

module.exports.router = serverless(app);