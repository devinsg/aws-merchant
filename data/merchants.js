const users = [
	/* Magic Number Soccer */
	{
		MerchantId: 1,
		FirstName: 'Eric',
		LastName: 'Cantona',
		Number: '7',
		FullName: 'Eric Cantona',
		Club: 'Manchester United'
	},
	{
		MerchantId: 2,
		FirstName: 'David',
		LastName: 'Beckham',
		Number: '7',
		FullName: 'David Beckham',
		Club: 'Manchester United'
	},
	{
		MerchantId: 3,
		FirstName: 'Cristiano',
		LastName: 'Ronaldo',
		Number: '7',
		FullName: 'Cristiano Ronaldo',
		Club: 'Manchester United'
	},
	{
		MerchantId: 4,
		FirstName: 'Michael',
		LastName: 'Owen',
		Number: '7',
		FullName: 'Michael Owen',
		Club: 'Manchester United'
	},
	{
		MerchantId: 5,
		FirstName: 'Angel',
		LastName: 'Di Maria',
		Number: '7',
		FullName: 'Angel Di Maria',
		Club: 'Manchester United'
	},
	{
		MerchantId: 6,
		FirstName: 'Memphis',
		LastName: 'Depay',
		Number: '7',
		FullName: 'Memphis Depay',
		Club: 'Manchester United'
	},
	{
		MerchantId: 7,
		FirstName: 'Alexis',
		LastName: 'Sanchez',
		Number: '7',
		FullName: 'Alexis Sanchez',
		Club: 'Manchester United'
	},
	{
		MerchantId: 8,
		FirstName: 'Antonio',
		LastName: 'Valencia',
		Number: '7',
		FullName: 'Antonio Valencia',
		Club: 'Manchester United'
	},
	{
		MerchantId: 9,
		FirstName: 'George',
		LastName: 'Best',
		Number: '7',
		FullName: 'George Best',
		Club: 'Manchester United'
	},
	/* US Presidents & Senators & Oligarchs */
	{
		MerchantId: 10,
		FirstName: 'Donald',
		LastName: 'Trump',
		FullName: 'Donald Trump',
		Title: 'Bilionaire and Oligarch',
		Position: 'current US President',
		POB: 'New York City'
	},
	{
		MerchantId: 11,
		FirstName: 'Abraham',
		LastName: 'Lincoln',
		FullName: 'Abraham Lincoln',
		Title: 'Statement, Politician, Lawyer',
		Position: 'Former US President',
		POB: 'Kentucky'
	},
	{
		MerchantId: 12,
		FirstName: 'Mitt',
		LastName: 'Romney',
		FullName: 'Mitt Romney',
		Title: 'Politician and Businessman',
		Position: 'US Governor from Utah',
		POB: 'Bloomfield Hills, Michigan'
	},
	{
		MerchantId: 13,
		FirstName: 'Condoleezza',
		LastName: 'Rice',
		FullName: 'Condoleezza Rice',
		Title: 'an American political scientist and diplomat',
		Position: 'US Secretary of State',
		POB: 'Birmingham, Alabama'
	},
	{
		MerchantId: 14,
		FirstName: 'Barack',
		LastName: 'Obama',
		FullName: 'Barack Obama',
		Title: 'an American attorney and politician',
		Position: 'Former US President',
		POB: 'Honolulu, Hawaii'
	},
	{
		MerchantId: 15,
		FirstName: 'Theodore',
		LastName: 'Roosevelt',
		FullName: 'Theodore Roosevelt',
		Title: 'an American statesman, politician, conservationist, naturalist, and writer',
		FullName: 'Theodore Roosevelt',
		Position: 'Former the 26th US President',
		POB: 'New York City'
	},
	{
		MerchantId: 16,
		FirstName: 'Dwight',
		LastName: 'Eisenhower',
		FullName: 'Dwight D. Eisenhower',
		Title: 'an American army general and statesman',
		Position: 'Former US President',
		POB: 'Denison, Texas'
	},
	{
		MerchantId: 16,
		FirstName: 'John',
		LastName: 'Kennedy',
		FullName: 'John F. Kennedy',
		Title: 'an American politician',
		Position: 'Former US President',
		POB: 'Brookline, Massachusetts'
	},
	{
		MerchantId: 17,
		FirstName: 'Henry',
		LastName: 'Kissinger',
		FullName: 'Henry Alfred Kissinger',
		Title: 'an American elder statesman, political scientist, diplomat, and geopolitical consultant',
		Position: 'Former United States Secretary of State and National Security Advisor',
		POB: 'Furth, Bavaria, Germany'
	},
	{
		MerchantId: 18,
		FirstName: 'Bill',
		LastName: 'Clinton',
		FullName: 'Bill Clinton',
		Title: 'an American politician',
		Position: 'Former United States President 42nd',
		POB: 'Arkansas'
	},
	{
		MerchantId: 19,
		FirstName: 'John',
		LastName: 'Rockefeller',
		FullName: 'John D. Rockefeller',
		Title: 'an American oil industry business magnate, industrialist, and philanthropist',
		Position: 'Founding and leading the Standard Oil Company',
		POB: 'New York'
	},
	{
		MerchantId: 20,
		FirstName: 'George',
		LastName: 'Bush',
		FullName: 'George W. Bush',
		Title: 'an American politician and businessman',
		Position: 'the 43rd president of the United States',
		POB: 'New Haven, Connecticut'
	},
	{
		MerchantId: 21,
		FirstName: 'Andrew',
		LastName: 'Carnegie',
		FullName: 'Andrew Carnegie',
		Title: 'a Scottish-American industrialist, business magnate, and philanthropist.',
		Position: 'King of Steel',
		POB: ' Dunfermline, Scotland'
	}
];

module.exports = users;