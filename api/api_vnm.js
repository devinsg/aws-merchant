const axios = require('axios');
const config = require('./config');

const API_URL = config.api;
const api = axios.create({
    baseURL: API_URL
});

api.defaults.headers.get['applicationId'] = config.applicationId;
api.defaults.headers.get['applicationSecret'] = config.applicationSecret;
api.defaults.headers.get['refreshToken'] = config.refreshToken;

module.exports = api;
