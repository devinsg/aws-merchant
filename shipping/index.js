'use strict';
const moment = require('moment');
const { login, queryTicketsWithStatus, shipperUpdateTicket } = require('../service/shippingService');

const handleError = (error, statusCode) => {
  let response = {
    statusCode: statusCode ? statusCode : 500,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(error),
  };
  return response;
};

module.exports.login = async (event, context) => {
  try
  {
    let req = JSON.parse(event.body);

    if(!req.username || !req.password) {
      throw { err_code: 'INVALID_USERNAME_PASSWORD' }
    }

    if(req.username != 'admin' || req.password != '@dmin') {
      throw { err_code: 'INVALID_USERNAME_PASSWORD' }
    }

    let data = await login(req.username, req.password);
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
  }
  catch (err) {
    return handleError(err);
  };
};

module.exports.getTicketsWithStatus = async (event, context) => {
  try
  {
    let query = 
    {
      skip: 0,
      id: '',
      ticket_id: '',
      customer: '',
      search: '', //keyword
      shipperId: [
        'shipperId'
      ],
      shipping_id: '',
      product: '',
      isProcessing: false,
      dateFrom: moment(new Date()).add(-1,'M').format('YYYY-MM-DD'),
      dateTo: moment(new Date()).add(1,'M').format('YYYY-MM-DD'),
      depots: {},
      limit: 50
    };

    let data = await queryTicketsWithStatus(query);
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
  }
  catch (err) {
    return handleError(err);
  };
};

module.exports.postTicketsWithStatus = async (event, context) => {
  try
  {
    let query = event.body ? JSON.parse(event.body) : 
    {
      skip: 0,
      id: '',
      ticket_id: '',
      customer: '',
      search: '', //keyword
      shipperId: [
        'shipperId'
      ],
      shipping_id: '',
      product: '',
      isProcessing: false,
      dateFrom: moment(new Date()).add(-1,'M').format('YYYY-MM-DD'),
      dateTo: moment(new Date()).add(1,'M').format('YYYY-MM-DD'),
      depots: {},
      limit: 50
    };

    let data = await queryTicketsWithStatus(query);
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
  }
  catch (err) {
    return handleError(err);
  };
};

module.exports.shipperUpdateTicket = async (event, context) => {
  try
  {
    let query = event.body ? JSON.parse(event.body) : {};
    let { ticket_id } = query;
    let data = await shipperUpdateTicket(ticket_id);
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
  }
  catch (err) {
    return handleError(err);
  };
};
