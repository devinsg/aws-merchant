const axios = require('axios');

const API_URL = 'https://kms3.kaiza.la/v1/';
const api = axios.create({
    baseURL: API_URL
});

api.defaults.headers.get['Content-Type'] = 'application/json';
api.defaults.headers.get['accessToken'] = '';

module.exports = api;